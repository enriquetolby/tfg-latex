\chapter{Requirement Analysis}
\label{chap:requirements}

\hfill\emph{``Crede quod habes, et habes.''}


\section{Overview}


This chapter describes one of the most important stages in software development: the requirement analysis using different scenarios. For this, a detailed analysis of the possible use cases is made using the Unified Modeling Language (UML). This language allows us to specify, build and document a system using graphic language. 

The result of this evaluation will be a complete specification of the requirements, which will be matched by each module in the design stage. This helps us also to focus on key aspects and take apart other less important functionalities that could be implemented in future works.

As commented before, our project aims to develop a personal assistance system that integrates the advantages of agent systems, information retrieval and Natural Language Processing. Our personal assistant should have the capability to solve user's questions using all of these features.



\section{Use cases}

These sections identify the use cases of the system. This helps us to obtain a complete specification of the uses of the system, and therefore define the complete list of requisites to match.  First, we will present a list of the actors in the system and a UML diagram representing all the actors participating in the different use cases. This representation allows, apart from specifying the actors that interact in the system, the relationships between them.

~\\
\noindent\textbf{Actors list}

\noindent The list of primary and secondary actors is presented in table \ref{tab:actores}. These actors participate in the different use cases, which are presented later.\\


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|c|x{6cm}|}
\noalign{\hrule height 2pt}
\textbf{Actor identifier} & \textbf{Role} & \textbf{Description}\tn
\hline
ACT-1 & User & System user.\tn
\hline
ACT-2 & Admin & System administrator.\tn
\hline
ACT-3 & Agent System & Intelligent agent system.\tn
\hline
ACT-4 & QA System & Question-Answer System.\tn
\hline
ACT-5 & Chatbot & Chatbot loaded into the system.\tn
\hline
ACT-6 & External service & External services for executing actions.\tn
\hline
ACT-7 & Linked Open Data Server & Server storing Linked Open Data.\tn
\hline
ACT-8 & Web Knowledge Base & Knowledge base available on the Web.\tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Actors list}
\label{tab:actores}
\end{table}


~\\
\noindent\textbf{UML diagram of use cases}

\noindent The UML diagram presented in Fig.~\ref{fig:req-usecasesc} represents all the described actors and how they participate in the different use cases. These use cases will be described the next sections, including each one a table with their complete specification. Using these tables, we will be able to define the requirements to be established.

\begin{figure}[h]
\centering
\includegraphics[width=12cm]{graficos-dia/req-usecasesc.png}
\caption{Representation of the natural language use cases}
\label{fig:req-usecasesc}
\end{figure}






\subsection{Proactive interaction (CU-1)}
Our personal assistant should be proactive. This means that, collecting the assertions extracted from the conversations and the user profile, it should create its own propositions or questions and send them to the user. For example, in the e-learning field, if a lesson has been already explained to the student, the personal assistant should propose him to revise this lesson the next time he accesses to the platform. 

Additional intelligence needs be implemented in order to support proactivity, so using intelligent agents becomes a requirement at this point. The summary of this use case in presented in table \ref{tab:cu1}.



\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-1\tn
\hline
\textbf{Name} &  Proactive interaction.\tn
\hline
\textbf{Description} & Support proactivity using intelligent agent technologies.\tn
\hline
\textbf{Actors} & Agent System, User. \tn
\hline
\textbf{Preconditions} & -\tn
\hline
\textbf{Usual flow} & \begin{enumerate}
						\item  The Agent System collects assertions about the user and the conversation.
                         \item Agent's plans according to these assertions are triggered, producing proactively an output that will be sent to the user.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & -
 \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 1}
\label{tab:cu1}
\end{table}



\subsection{Talk (CU-2)}
The user talks to the personal assistance system, and the Chatbot is able to maintain a conversation with the user. For this, natural language needs to be both understood and produced in the reply for the user. The summary of this use case in presented in Table \ref{tab:cu2}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-2\tn
\hline
\textbf{Name} & Talk.\tn
\hline
\textbf{Description} & Maintain a natural language conversation with the user.\tn
\hline
\textbf{Actors} & User, Chatbot.\tn
\hline
\textbf{Preconditions} &  The conversation corpus is loaded into the chatbot. 
						 \tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  The user sends a query to the system written in natural language. 
                         \item  The chatbot understands the user's query.
                         \item  The chatbot provides a natural language response for the user.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & -
 \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 2}
\label{tab:cu2}
\end{table}

\subsection{Answer a question (CU-3)}
The user sends his questions to the personal assistance system, and the QA System handles the task to answer them. The summary of this use case in presented in table \ref{tab:cu3}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-3\tn
\hline
\textbf{Name} & Answer a question.\tn
\hline
\textbf{Description} & Handle questions sent by the user and find a response for them.\tn
\hline
\textbf{Actors} & User, QA system.\tn
\hline
\textbf{Preconditions} & - \tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  The user sends a question to the system. 
                         \item  The QA system identifies the question.
                         \item  The QA system retrieves the needed information accessing to the knowledge base.
                         \item  The QA system generates the answer for the user.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & - \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 3}
\label{tab:cu3}
\end{table}

\subsection{Access knowledge base (CU-4)}
In order to find information, the knowledge base is accessed. Information can be fetched from the Linked Open Data Server. \ref{tab:cu4}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-4\tn
\hline
\textbf{Name} & Access knowledge base.\tn
\hline
\textbf{Description} & Access knowledge base and fetch information from the Linked Open Data Server.\tn
\hline
\textbf{Actors} & Linked Open Data Server.\tn
\hline
\textbf{Preconditions} &  The Linked Open Data Server contains information ready to be fetched. \tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  Information is fetched from the Linked Open Data Server and added to the knowledge base.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & - \tn

\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 4}
\label{tab:cu4}
\end{table}


\subsection{Execute an action (CU-5)}
The system should be able to trigger actions to be executed in external services, usually on user's demand. These external services usually run in a remote system, so this communication should be over a network. The summary of this use case in presented in Table \ref{tab:cu5}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-5\tn
\hline
\textbf{Name} &  Execute an action.\tn
\hline
\textbf{Description} & Execute actions by external services.\tn
\hline
\textbf{Actors} & User, External service. \tn
\hline
\textbf{Preconditions} & -
                        \tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  The user sends a query that requires executing an action.
                         \item  The request of execution is sent to the external services.
                         \item  The action is executed, and the possible output for the user is sent back.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & -
 \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 5}
\label{tab:cu5}
\end{table}

\subsection{Index (CU-6)}
Semi-structured information is indexed and then loaded into the Linked Open Data Service. The summary of this use case in presented in Table \ref{tab:cu6}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-6\tn
\hline
\textbf{Name} &  Index.\tn
\hline
\textbf{Description} & Index semi-structured information for the Linked Open Data Service.\tn
\hline
\textbf{Actors} & Admin, Linked Open Data Service. \tn
\hline
\textbf{Preconditions} & -\tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  The administrator provides semi-structured information to the indexer. 
                         \item  The information is indexed and loaded in the Linked Open Data Service.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & -
 \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 6}
\label{tab:cu6}
\end{table}

\subsection{Scraping (CU-7)}
The information on the Internet is normally presented in unstructured formats. Using scraping techniques, the information can be retrieved from from a web knowledge base and then converted to semi-structured format.
The summary of this use case in presented in Table  \ref{tab:cu7}.


\begin{table}[!htpb]
\centering
\begin{tabular}{|c|x{11cm}|}
\noalign{\hrule height 2pt}
\textbf{Identifier} & CU-7\tn
\hline
\textbf{Name} & Scraping.\tn
\hline
\textbf{Description} & Retrieve unstructured information from a web knowledge base on the Internet and convert it to semi-structured format.\tn
\hline
\textbf{Actors} & User, Web Knowledge Base. \tn
\hline
\textbf{Preconditions} & Web scrapper able to identify each content.\tn
\hline
\textbf{Usual flow} & \begin{enumerate}
                         \item  The administrator runs the scrapper over the website containing the desired web knowledge base.
                         \item  The scrapper fetches the pieces information in the web knowledge base and stores them with a semi-structured format.
                        \end{enumerate}\tn
\hline
\textbf{Alternative flow} & -
 \tn
\hline
\textbf{Postconditions} & - \tn
\noalign{\hrule height 2pt}
\end{tabular}
\caption{Use Case 7}
\label{tab:cu7}
\end{table}


\section{Conclusion: summary of requirements}
\label{sec:requirements}
In this chapter, we have studied the use cases that are applied to our project. Now, we can focus our study in some clear requirements presented next:

\begin{enumerate}
	

    \item Ability to understand natural language, as well as maintaining conversations in natural language with the user.
    
    
    \item Proactive interaction with the user by using intelligent agents. Proactivity means not only giving response to his queries, but also creating self-made propositions. 
    
    \item Apart from simple conversation, offering help by answering the user's questions about a subject depending on the application field. 
    
    \item Storing data in a knowledge base that holds information to  answer the user's questions. Possibility of quickly access this knowledge base to fetch a answer for the user.
    
    \item Executing actions in external services, in order to improve the reply for the user or to execute actuators on user's demand. 
    Heterogeneous but interchangeable end-points, providing modularity and a common interface for connecting new services.
    
    \item Possibility of scraping contents from unstructured knowledge bases on the Internet, converting them to a semi-structured format and then indexing and offering them in a Linked Open Data Service.

    \item Multiple user support, in order to offer the service to any number of users at the same time.
    
    

\end{enumerate}




