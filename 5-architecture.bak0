\chapter{Architecture}
\label{chap:architecture}

The architecture of the system is well divided in 3 different groups. First of all, Scrappy that will allow the automated discovery, witch objective is to enable users to access a wide amount of web components inside the OMR. The next component is an administration interface to manage all the widgets fetched by the former module and enable the administrator select the relevant ones. The last component consists of a client interface to enable the user find widgets and services that fit their requirements.

All of the components mentioned before have separated front-end and back-end modules.

In this chapter all the components will be described. As shown in figure \ref{fig:ArchitectureGeneral} the system consists in six main modules, two of them are front-end modules and the rest back-end modules.

The back-end modules are Scrappy (section \ref{sec:scrappy}), OMR/Sesame (section \ref{sec:omrsesame}), ranking module (section \ref{sec:rankingmodule}) and LMF (subsection \ref{subsec:omrclientback}). In the front-end we can found the OMR  Admin Interface (section \ref{sec:omradmin}) and OMR Developer Interface (subsection \ref{subsec:omrclientback}).

\begin{figure}[ht!]
\centering
\includegraphics[width=350pt]{graphics/ArchitectureGeneral.pdf}
\caption{General Architecture}
\label{fig:ArchitectureGeneral}
\end{figure}



\cleardoublepage
\section{Scrappy}
\label{sec:scrappy}

Scrappy\cite{fernandez2011semantic}  is a tool that allows extracting information from web pages and producing RDF data. Within the OMELETTE project, it has been used for developing the Discovery Module and mining several repositories of mashups, widgets and services in order to feed the Omelette Mashup Registry with these components.

Scrappy is developed using Ruby and it uses the Webkit library for visual processing of web resources.

Scrappy access a determined website, gets the corresponding HTML, converts the data into RDF using the defined ontology and inserts the result into the OMR (Figure \ref{fig:scrappysequence}).

\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_scrappy.pdf}
\caption{Scrappy sequence diagram}
\label{fig:scrappysequence}
\end{figure}

An example of mapping is shown next, which allows extracting all titles from www.elmundo.es:

\begin{lstlisting}[style=mono,breaklines=true, caption=Extractor example for www.elmundo.es]
dc: http://purl.org/dc/elements/1.1/
rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns#
sioc: http://rdfs.org/sioc/ns#
sc: http://lab.gsi.dit.upm.es/scraping.rdf#
*:
  rdf:type: sc:Fragment
  sc:selector:
    *:
      rdf:type: sc:UriSelector
      rdf:value: "http://www.elmundo.es/"
  sc:identifier:
    *:
      rdf:type: sc:BaseUriSelector
  sc:subfragment:
    *:
      sc:type: sioc:Post
      sc:selector:
        *:
          rdf:type: sc:CssSelector
          rdf:value: ".noticia h2, .noticia h3, .noticia h4"
      sc:identifier:
        *:
          rdf:type: sc:CssSelector
          rdf:value: "a"
          sc:attribute: "href"
      sc:subfragment:
        *:
          sc:type:     rdf:Literal
          sc:relation: dc:title
          sc:selector:
            *:
              rdf:type:  sc:CssSelector
              rdf:value: "a"
\end{lstlisting}

The above code is serialized using YARF format, supported by LightRDF gem, as well as RDFXML, JSON, NTriples formats, which can also be used to define the mappings.

Scrappy will use the Linked Mashup Ontology (LiMOn) when mapping the resources as seen in \ref{chap:limon}.

The total number of components scrapped can be consulted in tables \ref{tab:omrcomsum} and \ref{tab:omrcomrun}.

\begin {table}[ht!]
\caption {OMR components summary} \label{tab:omrcomsum} 
\begin{center}
\begin{tabular}{|c|c|}
\hline Number of services & 10194 \\ 
\hline Number of widgets & 1804 \\ 
\hline Number of applications & 7032 \\ 
\hline \textbf{Total number of components} & \textbf{11998} \\ 
\hline 
\end{tabular} 
\end{center}
\end{table}

\begin {table}[ht!]
\caption {Runnable components in OMR} \label{tab:omrcomrun} 
\begin{center}
\begin{tabular}{|c|c|}
\hline Number of runnable services & 1542 \\ 
\hline Number of runnable WSDL services & 296 \\ 
\hline Number of runnable REST services & 1246 \\ 
\hline Number of runnable widgets & 1804 \\ 
\hline \textbf{Total number of runnable components} & \textbf{3346} \\ 
\hline 
\end{tabular} 
\end{center}
\end{table}

\section{OMR / Sesame}
\label{sec:omrsesame}
The objective of the OMELETTE Mashup Registry is to provide an integrated centralized reference of web components to facilitate querying and selection of relevant ones when building new mashups. To achieve this, an RDF model based format is employed to describe the components. It is defined in this section along with the interface that supports querying and selection of components from the registry.

The registry integrates heterogeneous components that can be potentially used in various web applications. More specifically, mashup applications and services from the Web are the ones under consideration. Mashup is treated as a first-class object that is comprised of any web applications. Examples of mashups and services can be found in repositories such as Yahoo Pipes or Programmable Web.

In order to make these components available for developers, the registry stores relevant metadata that can be used by the developers for selecting components. Additionally, these metadata should be available in the web in order to make it possible to automate the population of the registry with real components. Usually, web component repositories contain metadata such as a component's name, textual description, tags or categorization.

For developing purposes Sesame is used instead of the OMR. Both Sesame and OMR complies the same REST API interface.

Sesame is a de-facto standard framework for processing RDF data. This includes parsing, storing, inferencing and querying of/over such data. It offers an easy-to-use API that can be connected to all leading RDF storage solutions.

Sesame has been designed with flexibility in mind. It can be deployed on top of a variety of storage systems (relational databases, in-memory, file systems, keyword indexers, etc.), and offers a large scale of tools to developers to leverage the power of RDF and related standards. Sesame fully supports the SPARQL query language for expressive querying and offers transparent access to remote RDF repositories using the exact same API as for local access. Finally, Sesame supports all main stream RDF file formats, including RDF/XML, Turtle, N-Triples, TriG and TriX.

\section{Ranking module}
\label{sec:rankingmodule}
Nowadays, developers of web application mashups face a sheer overwhelming variety and pluralism of web services. Therefore, choosing appropriate web services to achieve specific goals requires a certain amount of knowledge as well as expertise. In order to support users in choosing appropriate web services it is not only important to match their search criteria to a dataset of possible choices but also to rank the results according to their relevance, thus minimizing the time it takes for taking such a choice. Therefore, we implemented the three of the 6 ranking approaches investigated\cite{Tilo12} in an empirical manner.

The three raking functions implemented in this scenario are:

\begin{itemize}
\item \textit{Degree centrality}, i.e. in our scenario the number of mashups that use a certain web service, witch directly reflects its popularity. It will be represented by the limon:DegCent property.
\item \textit{Closeness centrality}: A vertex $ v $ is ranked higher the sorter the geodistic distance between itself and other vertices are, i.e. the \textit{closer} it is to other vertices. Closeness centrality is also an important technique in social network analysis and can be determined with the help of Brandes'\cite{brandes2001faster} algorithm. In order to be working with our graph structure we implemented it with a modification \cite{opsahl2010node} proposed. It is represented by the limon:ClosCent property.
\item \textit{GSO}: The amount of hits the Google Search Engine\footnote{http://www.google.com} returned querying it for the web service's name and limiting the results to the domain of \textit{StackOverFlow}\footnote{A question-and-answer website specialized on programming topics, is an indicator of how widespread a web service is among developers. http://stackoverflow.com}. "Twitter site:stackoverslow.com" could serve an example for such a search engine query. This ranking function result is represented by the limon:RatSoc property.
\end{itemize}

This module is implemented as a separate component written in Java witch runs independently. It connects to the OMR or Sesame and executes the algorithms to calculate the indicators in each component. It is scheduled to run periodically and re-calculate the indexes when new mashups or services are incorporated to the meta-directory.

\begin{figure}[h]
\centering
\includegraphics[width=200pt]{graphics/Diagrama_secuencia_ranking.pdf}
\caption{Sequence diagram for ranking index generation}
\label{fig:rankingsequence}
\end{figure}

\newpage

\section{OMR Admin Interface}
\label{sec:omradmin}
The OMR administrator interface is  using PHP\footnote{http://www.php.net}, HTML, CSS and Javascript.
A general view of the class diagram is showd in \ref{fig:admininterface}.
In this architecture the main index.php orchestras all the actions. In order to work in a similar way as SIMILE Exhubit\footnote{http://simile-widgets.org/exhibit3/} does with its facets the model and the view is not completely separated.


\begin{figure}[ht!]
\centering
\includegraphics[width=400pt]{graphics/omr-interface-architecure.pdf}
\caption{OMR Admin Interface architecture}
\label{fig:admininterface}
\end{figure}

\FloatBarrier

\subsection{Main view: index.php}
In the index.php the developer can decide how to visualize everythings and just adding few lines of code it will generate the facet boxes and the results cointainer.
It is mostly HTML with CSS and Javascript. It also uses the javascript framework jQuery\footnote{http://jquery.com} and svglizer\footnote{http://sgvizler.googlecode.com/} to render the result of SPARQL SELECT queries into charts.
The functions avaiable that will generate the HTML automatically are described in functions.omelette.php.

\begin{figure}[ht!]
\centering
\includegraphics[width=400pt]{graphics/omr-admin.png}
\caption{OMR Admin Interface}
\label{fig:admininterfacemain}
\end{figure}
\subsection{Functions library}
The file functions.omelette.php is a library of functions used by the rest of the classes. 



  \subsubsection{Generate facet boxes}
  \begin{lstlisting}[breaklines=true, style=mono, caption="Creating facet box"]
<?php get_limon_block("limon:categorizedBy",filter); ?>
\end{lstlisting}

As shown in figure \ref{fig:generatingblocks} all the facet boxes will be rendered.

\begin{figure}[h]
\centering
\includegraphics[width=400pt]{graphics/generatingblocks.png}
\caption{Generating Facet Boxes}
\label{fig:generatingblocks}
\end{figure}


  \subsubsection{Generate results list}
  This will generate the container for the mashup and service list results matching the selected facets.
    \begin{lstlisting}[breaklines=true, style=mono, caption="Creating result container"]
<?php get_results(filter,order,dir); ?>
\end{lstlisting}

  \subsubsection{Generate mashup or service information}
  And the view generated when a mashup is selected is easily generated as it follows
    \begin{lstlisting}[breaklines=true, style=mono, caption="Creating information view"]
				<?php elseif( isset(_GET['uri']) ): ?>
					<?php get_predicates(_GET['uri']); ?>				
				<?php endif; ?>
\end{lstlisting}

  \subsubsection{Generate charts}
  It uses an external module named svglizer to generate charts. The system will generate the proper SPARQL SELECT query and insert it into the module to generate the pie chart.
    \begin{lstlisting}[breaklines=true, style=mono, caption="Generating pie charts"]
<div id="chart"
      data-sgvizler-endpoint="http://krusti.gsi.dit.upm.es:8080/LMF-3.0.0/sparql/select"
      data-sgvizler-query="<?php echo base64_decode(_GET['query']); ?>"
      data-sgvizler-chart="gPieChart"
      data-sgvizler-endpoint_output="xml" 
      style="width:800px; height:400px;"></div
	</body>
\end{lstlisting}

\begin{figure}[h]
\centering
\includegraphics[width=300pt]{graphics/piechart.png}
\caption{Pie chart}
\label{fig:piechart}
\end{figure}

To generate a chart to visualize statistics, the user has to browse trough the OMR administrator, selecting filters and making search queries using free text. When the user is satisfied with the results showed he can cick on the button to generate the pertinent chart. The flow can be visualized in figure \ref{fig:chartsequence}.

\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_graficas.pdf}
\caption{Sequence diagram for chart generation}
\label{fig:chartsequence}
\end{figure}

\subsubsection{Generate mashup or service information}
And the view generated when a mashup is selected is easily generated as it follows
\begin{lstlisting}[breaklines=true, style=mono, caption="Creating information view"]
	<?php elseif( isset(_GET['uri']) ): ?>
		<?php get_predicates(_GET['uri']); ?>				
	<?php endif; ?>
\end{lstlisting}


\subsubsection{Admin authentication}
It is in charge of handeling the authentication wich is HTTP based.\footnote{http://php.net/manual/es/features.http-auth.php}

\subsubsection{Cache and performance}
It is also in charge of performance tweaks using file based cache. When a query to the OMR is executed (Figure \ref{fig:cachesequence_a}) it stores the result shown in \ref{lst:cachecat} using a no collision hash calculated using the SPARQQL query. Every time a query is launched it will look for a stored result in the cache file list \ref{lst:cachels} and it will be served in case there is a matching (Figure \ref{fig:cachesequence_b}). The cached result is parsed the same way using sparqllib.
This is quite important when working with a repository with thousands of services that will demand high processing time for all of them to be retrieved and filtered.

\begin {table}[ht!]
\caption {Execution query time comparison} \label{tab:timecomp} 
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline  & OMR Quering & Local Sesame & Cached query \\ 
\hline Maximun time & 120 seconds & 15 seconds & ~0 seconds \\ 
\hline Minimun time & 3 secods & 1 second & ~0 seconds \\ 
\hline 
\end{tabular}
\end{center}
\end{table}


\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_cache_A.pdf}
\caption{Sequence diagram if query is not in cache}
\label{fig:cachesequence_a}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_cache_B.pdf}
\caption{Sequence diagram if query cached}
\label{fig:cachesequence_b}
\end{figure}
\FloatBarrier
\lstinputlisting[caption={Cache file list example},label={lst:cachels} ]{code/cache_ls.txt}
\lstinputlisting[caption={Cache file out example},label={lst:cachecat} ]{code/cache_cat.txt}

\subsubsection{Repository wrapper}
The class omr.php allows allows the system to connect to the different repositories available providing an API to it.
\begin{itemize}
\item connect(), connects to the repository endpoint.
\item query(), executes a query in SPARQL language
\item insert(), inserts a new element into the repository.
\end{itemize}

The posible repositories are the OMR, Sesame and LMF.\footnote{Linked Media Framework}


\subsubsection{Sparql Library}
In order to handle RDF procesing and SPARQL queries responses parsing, the system uses a third party library, sparqllib.php\footnote{http://graphite.ecs.soton.ac.uk/sparqllib/}.

It has been modified to allow queries using POST method and HTTP authentication (witch is a requisite for connecting to the OMR).

The library provides functions very similar to PHP mysql\_* for comfort.

\lstinputlisting[label=sparqllibsample,caption=Sparqllib usage example ]{code/sparqllib_example.php}

\begin {table}[ht!]
\caption {Sparqllib output example} \label{tab:sparqllibsample} 
\begin{center}
\begin{tabular}{|c|c|}
\hline 
Person & Name \\ 
\hline 
bf4120a00000000 & Bob \\ 
\hline 
bf4120a1200000e & Alice \\ 
\hline 
\end{tabular}
\end{center}
\end{table}
The output is shown in Table \ref{tab:sparqllibsample}

\newpage

\subsubsection{Validating resource}
The users browses into the repository trough the OMR admin interface going into the "Browse pending" section (Figure \ref{fig:admintopmenu}), selecting filters from the facet boxes (Figure \ref{fig:generatingblocks}) and using the free text search box (Figure \ref{fig:searchboxadmin}) finally reaches a resource to be validated. 

\begin{figure}[h]
\centering
\includegraphics[width=200pt]{graphics/admin-top-menu.png}
\caption{OMR admin top menu}
\label{fig:admintopmenu}
\end{figure}

When selecting the validate button (Figure \ref{fig:validatebutton}), a SPARQL update query will be executed using the Sparql library to modify the OMR to indicate that the selected resource is already validated. Additionally, the RDF resource will be stored into the LMF to be indexed by SOLR.

\begin{figure}[h]
\centering
\includegraphics[width=200pt]{graphics/omr-validate-buttons.png}
\caption{OMR admin validate and reject buttons}
\label{fig:validatebutton}
\end{figure}

Then, the validated services are shown in the \textit{Show approved} section that can be accessed through the top menu as seen in Figure \ref{fig:admintopmenu}.

The whole sequence is shown in Figure \ref{fig:validatesequence}.
%\begin{figure}[h]
%\centering
%\includegraphics[width=350pt]{graphics/validar_servicio.pdf}
%\caption{Activity diagram for validating service}
%\label{fig:validateactivity}
%\end{figure}


\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_validar.pdf}
\caption{Sequence diagram for validating RDF resource}
\label{fig:validatesequence}
\end{figure}
\newpage
\subsubsection{Rejecting resource}
The users browses into the repository trough the OMR admin interface going into the "Browse pending" section (Figure \ref{fig:admintopmenu}), selecting filters from the facet boxes (Figure \ref{fig:generatingblocks}) and using the free text search box (Figure \ref{fig:searchboxadmin}) finally reaches a resource to be validated. When selecting the reject button (Figure \ref{fig:validatebutton}), a SPARQL update query will be executed using the Sparql library to modify the OMR to indicate that the selected resource has been rejected by the administrator.

\begin{figure}[h]
\centering
\includegraphics[width=100pt]{graphics/omr-search-box.png}
\caption{OMR admin search box}
\label{fig:searchboxadmin}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_rechazar.pdf}
\caption{Sequence diagram for rejecting RDF resource}
\label{fig:rejectsequence}
\end{figure}

\newpage
\subsubsection{Wadl generation}
The system can generate a WADL\footnote{ is a machine-readable XML description of HTTP-based web applications (typically REST web services)} file for those services with rosm\footnote{http://www.wsmo.org/ns/rosm/0.1/} description.
\textbf{Important}: This is only available for services scrapped from Yahoo Pipes.

It returns a WADL file where it is specified the target URL and parameters for the REST service.
This data is easily fetched executing SPARQL query as shown in \ref{lst:wadlsparql}.
\newpage
 \begin{lstlisting}[breaklines=true, style=mono, caption={Retrieving information for WADL with SPARQL}, label={lst:wadlsparql}]
SELECT DISTINCT ?parameter ?getUrl WHERE{
	 uriresource limon:describedBy ?a .
	 ?a rosm:supportsOperation ?b .
	 ?b hrests:hasAddress ?getUrl .
	 ?b rosm:requestURIParameter ?c .
	 ?c rdfs:label ?d .
	 ?d rdfs:label ?parameter	 
	}
\end{lstlisting}

An example of WADL output is shown in \ref{lst:wadlexample}.


\lstinputlisting[label=example-wadl,caption=WADL example,label={lst:wadlexample} ]{code/example-wadl.xml}

To generate WADL, the user has to browse trough the OMR Admin and select a mashup or service. Afterwards, if it is supported by the selected service, the user can request the OMR Admin to generate a WADL file. This sequence is described in figure \ref{fig:wadlsequence}.

\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_wadl.pdf}
\caption{Sequence diagram for WADL file generation}
\label{fig:wadlsequence}
\end{figure}

\FloatBarrier

\subsubsection{LMF integration}
There is an extra module\footnote{lmf.php} that adapts the queries to insert selected services into an LFM repository. This enables SOLR indexing for the client service browser.

It connects to the sparql/update endpoint of LMF, in wich SPARQL insert queries can be executed.


\section{OMR Client Browser}
\label{sec:omrclientbrowser}

The OMR Client Browser (\ref{fig:omrclient}) consists in two parts, a backend formed by LMF, and a front-end written using web technologies such as javascript and HTML.
It is based on the Episteme\footnote{Episteme is a creator of opportunities designed to link the various partners and create consortia to suit a particular offer. With semantic search is able to find companies that are the best suited to an opportunity based on their characteristics.} platform.

The user can create a new search witch will be automatically saved for posterity. The user will be shown a form with auto complete fields (witch will retrieve the autocomplete data automatically (step 2 in Figure \ref{fig:clientsequence}). The user selects the desired filters (step 3 in Figure \ref{fig:clientsequence}). A HTTP request to the LMF is made to retrieve the search results (step 4 in Figure \ref{fig:clientsequence}). If any of the fields was semantic, an extra HTTP request will be done to the semantic module. The results are shown to the user (step 5 in Figure \ref{fig:clientsequence}).
\begin{figure}[h]
\centering
\includegraphics[width=350pt]{graphics/Diagrama_secuencia_client.pdf}
\caption{Scrappy sequence diagram}
\label{fig:clientsequence}
\end{figure}

All the modules are described in the following points.

%TODO hacer mas alta esta imagen y menos ancha
\begin{figure}[ht!]
\centering
\includegraphics[width=460px]{graphics/omrclient.png}
\caption{OMR client interface}
\label{fig:omrclient}
\end{figure}



\subsection{Back-end}
\label{subsec:omrclientback}
For the back-end the system uses Linked Media Framework to build semantic search over the approved data selected with the OMR Admin described in \ref{sec:omradmin}.
The only thing needed to configure the LMF is a semantic core that should follow the rules showed in \ref{lst:semcore}. A search core representes a specific index configuration with fields filled from the linked data cloud. It consisits of two mayor elements: a filter deciding witch resources are added to the search index, and one or more fields defining the index fields of the search core and how the values of these fields are calculated.
These filters are written following LD Path\footnote{http://code.google.com/p/ldpath/}, a simple path-based query language, similar to SPARQL Property Paths, that is particularly well-suited for querying and retrieving resources from the Linked Data Cloud by following RDF links between resources and servers.

\newpage

 \begin{lstlisting}[breaklines=true, style=mono, caption={Semantic core configuration for LMF using LD Path}, label={lst:semcore}]
@prefix limon : <http://www.ict-omelette.eu/schema.rdf#> ;
@prefix ctag : <http://commontag.org/ns#>;
  
  Rdftype = rdf:type :: xsd:string ;
  categorizedBy = limon:categorizedBy :: xsd:string ;
  api = limon:api :: xsd:string ;
  sslSupport = limon:sslSupport :: xsd:string ;
  label = rdfs:label :: xsd:string ;
  protocol = limon:protocol :: xsd:string ;
  tagged = ctag:tagged :: xsd:string ;
  authentication = limon:authentication :: xsd:string ;
  dataFormat = limon:dataFormat :: xsd:string ;
  description = dc:description :: xsd:string ;
  Provenance = "gsi" :: xsd:string ;
\end{lstlisting}

\subsection{Front-end}
\label{subsec:omrclientfront}
The Episteme front-end has been developed using various javascript frameworks, such as knockoutjs witch simplifies the dymanic javascript UI with the Model-View-View Model (MVVM) pattern, and KendoUI\footnote{http://www.kendoui.com} to build beautiful interactive interface.
Creating a search engine with Episteme has been done following few steps.

\paragraph{Search fields} If we want to show new search fields as shown in figure \ref{fig:searchfields} first of all we have to construct the SPARQL query that will fill the auto-complete. For example, one of them should look like described in \ref{lst:sparqlauto}. It is also necessary to add the data binding sequence with knockout as shown in \ref{lst:databinding}.

\newpage

\begin{lstlisting}[breaklines=true, style=mono, caption={SPARQL to retrieve all component types}, label={lst:sparqlauto}]
SELECT DISTINCT ?o  
  WHERE { ?a rdf:type ?o . } 
  GROUP BY ?o ORDER BY DESC(?o) 
}
\end{lstlisting}

\begin{figure}[h]
\centering
\includegraphics[width=300px]{graphics/searchfields.png}
\caption{Search fields in OMR client interface}
\label{fig:searchfields}
\end{figure}
\begin{lstlisting}[breaklines=true, style=mono, caption={Data binding with knockout}, label={lst:databinding}]
<!-- RDF TYPE -->
    <span data-bind="if: data.field() == 'Rdftype'">
      <span class="filterName" data-bind="text: root.lang().rdftype"></span>
      <input class="solrInput" data-bind="kendoComboBox: { data: root.Rdftype, value: root.selectedRdftype}" />
      <a class="greenButton" data-bind="click: root.addSolrFilter.bind(data, data.values, root.selectedRdftype)">+</a>
      <a class="filterInfo blueButton" data-bind="click: root.addSolrFilter.bind(data, data.values, root.selectedType),attr: { 'title': root.lang().rdftypeHelp}">?</a>
    </span>
\end{lstlisting}
\paragraph{Multilingual support}Episteme is prepared to enable multi-language, all the string values are stored in the \textit{dictionary.js}.
\paragraph{Personalization manual} is available with full detail at the Episteme Wiki.\footnote{https://github.com/gsi-upm/Episteme/wiki/Create-a-custom-search-engine-with-Episteme}


\section{Conclusions}
We have shown a architecture fully modular in witch each component can be developed, maintained and deployed separately.

The automated discovery system could be upgraded to acquire  new functionalities and adapt to new web structure.

As we demonstrated the OMR module could be substituted by an other framework to store the RDF data such as Sesame as we built the communication interface complying the standards.

Using modules built using the Model-View-View Model patters makes them reusable and extended for other purposes.

All modules are available as open source projects.
