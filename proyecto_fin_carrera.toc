\contentsline {chapter}{Resumen}{VII}{chapter*.3}
\contentsline {chapter}{Abstract}{IX}{chapter*.5}
\contentsline {chapter}{Agradecimientos}{XI}{chapter*.7}
\contentsline {chapter}{Contents}{XIII}{section*.8}
\contentsline {chapter}{List of Figures}{XVII}{section*.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.12}
\contentsline {section}{\numberline {1.1}Context}{1}{section.13}
\contentsline {section}{\numberline {1.2}Project goals}{2}{section.14}
\contentsline {section}{\numberline {1.3}Structure of this document}{3}{section.15}
\contentsline {chapter}{\numberline {2}Enabling Technologies}{5}{chapter.16}
\contentsline {section}{\numberline {2.1}Introduction}{5}{section.17}
\contentsline {section}{\numberline {2.2}Cognitive Computing}{6}{section.18}
\contentsline {subsection}{\numberline {2.2.1}Cognitive computing Properties}{6}{subsection.21}
\contentsline {section}{\numberline {2.3}Task Automation}{7}{section.22}
\contentsline {section}{\numberline {2.4}Ewetasker}{8}{section.23}
\contentsline {section}{\numberline {2.5}Conversation Tools}{8}{section.24}
\contentsline {subsection}{\numberline {2.5.1}Api.ai}{9}{subsection.25}
\contentsline {subsection}{\numberline {2.5.2}IBM Watson Conversation}{10}{subsection.27}
\contentsline {subsubsection}{\numberline {2.5.2.1}IBM Watson Text to Speech}{11}{subsubsection.30}
\contentsline {subsubsection}{\numberline {2.5.2.2}IBM Watson Speech to Text}{12}{subsubsection.36}
\contentsline {section}{\numberline {2.6}Google Home}{12}{section.38}
\contentsline {section}{\numberline {2.7}Beacons}{13}{section.40}
\contentsline {section}{\numberline {2.8}Woowee MiP Robot}{14}{section.42}
\contentsline {subsection}{\numberline {2.8.1}Technologies implemented}{14}{subsection.43}
\contentsline {subsection}{\numberline {2.8.2}Response Actions}{15}{subsection.45}
\contentsline {chapter}{\numberline {3}Requirement Analysis}{17}{chapter.46}
\contentsline {section}{\numberline {3.1}Introduction}{17}{section.47}
\contentsline {section}{\numberline {3.2}Use cases}{17}{section.48}
\contentsline {subsection}{\numberline {3.2.1}System actors}{18}{subsection.49}
\contentsline {subsection}{\numberline {3.2.2}Use cases}{19}{subsection.50}
\contentsline {subsubsection}{\numberline {3.2.2.1}Welcome Use Case}{20}{subsubsection.52}
\contentsline {subsubsection}{\numberline {3.2.2.2}Reminder Use Case}{21}{subsubsection.54}
\contentsline {subsection}{\numberline {3.2.3}Conclusions}{21}{subsection.56}
\contentsline {chapter}{\numberline {4}Architecture}{23}{chapter.57}
\contentsline {section}{\numberline {4.1}Introduction}{23}{section.58}
\contentsline {section}{\numberline {4.2}Overview}{23}{section.59}
\contentsline {section}{\numberline {4.3}Conversation system}{25}{section.61}
\contentsline {subsection}{\numberline {4.3.1}Google Home}{25}{subsection.62}
\contentsline {subsection}{\numberline {4.3.2}Api.ai}{26}{subsection.63}
\contentsline {subsubsection}{\numberline {4.3.2.1}App Module}{27}{subsubsection.65}
\contentsline {subsubsection}{\numberline {4.3.2.2}Action Manager}{29}{subsubsection.100}
\contentsline {subsubsection}{\numberline {4.3.2.3}Reminder Manager}{29}{subsubsection.101}
\contentsline {subsection}{\numberline {4.3.3}IBM Watson}{30}{subsection.102}
\contentsline {subsubsection}{\numberline {4.3.3.1}App Module}{31}{subsubsection.104}
\contentsline {subsubsection}{\numberline {4.3.3.2}Conversation Module}{32}{subsubsection.106}
\contentsline {subsubsection}{\numberline {4.3.3.3}Text To Speech Module}{32}{subsubsection.130}
\contentsline {subsubsection}{\numberline {4.3.3.4}Speech To Text Module}{33}{subsubsection.131}
\contentsline {section}{\numberline {4.4}Ewetasker server}{33}{section.132}
\contentsline {subsection}{\numberline {4.4.1}Channels Created}{34}{subsection.133}
\contentsline {subsubsection}{\numberline {4.4.1.1}Time Channel}{35}{subsubsection.135}
\contentsline {subsubsection}{\numberline {4.4.1.2}Robot Channel}{35}{subsubsection.136}
\contentsline {subsection}{\numberline {4.4.2}Sensors involved}{35}{subsection.137}
\contentsline {subsection}{\numberline {4.4.3}Actuators involved}{36}{subsection.138}
\contentsline {section}{\numberline {4.5}Proxy Server Module}{36}{section.139}
\contentsline {subsection}{\numberline {4.5.1}Action Trigger}{36}{subsection.140}
\contentsline {subsection}{\numberline {4.5.2}Control MiP}{37}{subsection.143}
\contentsline {subsection}{\numberline {4.5.3}Daemon Time}{38}{subsection.145}
\contentsline {section}{\numberline {4.6}Mobile Application}{38}{section.146}
\contentsline {chapter}{\numberline {5}Case study}{39}{chapter.147}
\contentsline {section}{\numberline {5.1}Introduction}{39}{section.148}
\contentsline {section}{\numberline {5.2}Welcome Use Case}{39}{section.149}
\contentsline {section}{\numberline {5.3}Reminder Use Case}{42}{section.153}
\contentsline {section}{\numberline {5.4}Task Automation Use Case}{43}{section.155}
\contentsline {section}{\numberline {5.5}Context Awareness Use Case}{44}{section.157}
\contentsline {section}{\numberline {5.6}Conclusions}{45}{section.159}
\contentsline {chapter}{\numberline {6}Conclusions and future work}{47}{chapter.160}
\contentsline {section}{\numberline {6.1}Introduction}{47}{section.161}
\contentsline {section}{\numberline {6.2}Conclusions}{47}{section.162}
\contentsline {section}{\numberline {6.3}Api.ai vs IBM Watson}{48}{section.163}
\contentsline {section}{\numberline {6.4}Achieved goals}{49}{section.164}
\contentsline {section}{\numberline {6.5}Problems faced}{49}{section.165}
\contentsline {section}{\numberline {6.6}Future work}{50}{section.166}
\contentsline {chapter}{Bibliography}{I}{section*.167}
